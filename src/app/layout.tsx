'use client'

import { Inter } from "next/font/google";
import "./globals.css";

import Link from 'next/link';
import {
  DarkThemeToggle,
  Flowbite,
  Footer,
  Navbar,
  ThemeModeScript,
} from "flowbite-react";
import { usePathname } from "next/navigation";

const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
  children
}: Readonly<{
  children: React.ReactNode;
}>) {
  const navBarCollapsed = "absolute bg-white dark:bg-slate-800 border border-solid dark:border-slate-600 px-2 rounded right-8 shadow-md top-12 ml-auto w-32 z-10";
  const navBarUncollapsed = "md:border-none md:px-0 md:shadow-none md:static";

  const path = usePathname();

  return (
    <html lang="en">
      <Flowbite>
        <head>
          <ThemeModeScript />
        </head>
        <body className={inter.className}>
          <div className="dark:text-white h-screen overflow-y-hidden">
            <Navbar className="border-b-2 sticky">
              <Navbar.Brand href="/">
                 NyxTrail
              </Navbar.Brand>
              <Navbar.Toggle />
              <Navbar.Collapse className={navBarCollapsed + " " + navBarUncollapsed}>
                <Navbar.Link
                  active={path === '/'}
                  className="border-none flex items-center h-full"
                  href='/' >
                  Home
                </Navbar.Link>
                <Navbar.Link
                  active={path === '/projects'}
                  className="border-none flex items-center h-full"
                  href='/projects' >
                  Projects
                </Navbar.Link>
                <Navbar.Link
                  active={path === '/resume'}
                  className="border-none flex items-center h-full"
                  href="/resume" >
                  Resume
                </Navbar.Link>
                <DarkThemeToggle className="p-auto" />
              </Navbar.Collapse>
            </Navbar>
            
            <main className="h-full w-full items-start overflow-scroll dark:bg-gray-800">
              
              {children}
            </main>
            
            <Footer className="border-t bottom-0 md:h-[50px] lg:h-[50px] py-2 rounded-none sticky">
              <div className="text-xs w-full text-center">
                NyxTrail&apos;s website. Built with <Link href="https://react.dev" className="text-sky-700">React</Link> and&nbsp;
                <Link className="text-sky-700" href="https://flowbite-react.com">Flowbite React</Link>.
              </div>
            </Footer>
          </div>
          
        </body>
      </Flowbite>
    </html>
  );
}
