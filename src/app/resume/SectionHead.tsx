import {
  Text,
  View,
} from "@react-pdf/renderer";
import { tw } from "../components/tw";

type SectionHeadInput = {
  title: string
};

export const SectionHead = ({title}: SectionHeadInput) => (
  <View>
    <Text style={tw("font-lato font-bold text-lg pb-2")}>
      {title}
    </Text>
  </View>
);
