import {
  Text,
  View,
} from "@react-pdf/renderer";
import { SubHead } from "./SubHead";
import { tw } from "../components/tw";
import { List, ListItem } from "./components/List";
import { ReactElement } from "react";

type ExperienceObjectInput = {
  company: string,
  duration: string,
  role: string,
  tasks: ReactElement[],
  title: string,
};

export const ExperienceObject = ({
  company,
  duration,
  role,
  tasks,
  title,
}: ExperienceObjectInput) => (
  <View>
    <SubHead title={ title } />
    <View style={tw("pl-7 text-sm")}>
      <View style={tw("flex-row")}>
        <Text style={tw("font-bold")}>Role: </Text><Text>{role}</Text>
      </View>
      <View style={tw("flex-row")}>
        <Text style={tw("font-bold")}>Duration: </Text><Text>{duration}</Text>
      </View>
      <View style={tw("flex-row")}>
        <Text style={tw("font-bold text-sm")}>Payroll Company: </Text><Text>{company}</Text>
      </View>
      <Text style={tw("font-bold text-sm")}>Tasks: </Text>
      <View style={tw("ml-2")}>
        <List>
          {
            tasks.map((task, index) => {
              return (
                <ListItem key={index} mkey={index}>
                  {task}
                </ListItem>
              );
            })
          }
      </List>
      </View>
    </View>
  </View>
);
