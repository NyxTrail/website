import {
  Text,
  View,
} from "@react-pdf/renderer";
import { ExperienceObject } from "./ExperienceObject";
import { SectionHead } from "./SectionHead";

export const ProjectsAndExperience = () => (
  <View>
    <SectionHead title="Projects and Experience" />
    <ExperienceObject
      title="Packaging applications for Debian GNU/Linux"
      duration="January 2024 to Current"
      company="Volunteer Developer"
      role="Sponsored Maintainer"
      tasks={[
        <Text key={0}>I am currently contributing to the Debian GNU/Linux project as a packager </Text>,
        <Text key={1}>Packaged and uploaded Hyprland (an application for GNU/Linux); currently awaiting
        approval and sponsorship.</Text>
      ]}
    />
    <ExperienceObject
      title="Textile Lens"
      role="Backend Developer"
      duration="July 2023 to Sep 2023"
      company="Freelancer"
      tasks={[
        <Text key={0}>Developed user authentication using Amazon Cognito</Text>,
        <Text key={1}>Set up SMS based authentication using custom Lambda functions</Text>,
        <Text key={2}>Developed CloudFormation templates for configuring AWS resources</Text>,
        <Text key={3}>Built GraphQL based APIs using AWS Appsync</Text>,
        <Text key={4}>Worked with DynamoDB as a data source for the GraphQL API</Text>,
        <Text key={5}>Developed a React based admin panel for managing the application</Text>,
        <Text key={6}>Set up S3 as storage for various assets (images, videos) related to the project</Text>,
        <Text key={7}>Backend and admin panel both version controlled with git</Text>,
      ]}
    />
    <ExperienceObject
      title="Aksharam"
      role="Android Developer"
      duration="Jan 2022 to June, 2023"
      company="Self Employed, Open Source Project"
      tasks={[
        <Text key={0}>Developed android application in Java/Kotlin</Text>,
        <Text key={1}>Deployed application to Google Play Store and F-Droid</Text>,
        <Text key={2}>Designed and developed application theme based on Material Design</Text>,
        <Text key={3}>Research and gathered data regarding Indic language scripts for the application</Text>,
      ]}
    />
    <ExperienceObject
      title="Balanced 3D"
      role="Video Game Developer"
      duration="Jan 2021 to Sep 2021"
      company="Self Employed"
      tasks={[
        <Text key={0}>Built my first video game, &ldquo;Balanced 3D&rdquo;, for android devices</Text>,
        <Text key={1}>Balanced 3D is an arcade, physics-based game about moving balls through a maze while collecting points</Text>,
        <Text key={2}>The game was build using the Godot game engine and the GDScript programming language</Text>,
        <Text key={3}>Implemented physics and collision using GDScript</Text>,
        <Text key={4}>Designed and developed 3D assets and art for the game using Blender</Text>,
        <Text key={5}>Designed and developed multiple levels/stages for the game</Text>,
        <Text key={6}>Developed a scoring system for the game</Text>,
      ]}
    />
    <ExperienceObject
      title="AWS Cloud Engineer"
      role="Cloud Engineer"
      duration="June 2016 to Dec 2020"
      company="Amazon Web Services (AWS)"
      tasks={[
        <Text key={0}>Developed and maintained AWS Lambda functions in Node.js (Javascript), Java, Python</Text>,
        <Text key={1}>Developed CloudFormation templates for easy deployment of serverless applications</Text>,
        <Text key={2}>Developed and debugged Terraform templates to deploy AWS applications into the cloud</Text>,
        <Text key={3}>Develop custom Lambda functions using Bash and other unsupported languages</Text>,
        <Text key={4}>Optimise Lambda functions for efficient memory and CPU utilisation</Text>,
        <Text key={5}>Integrated Lambda functions with Amazon API Gateway</Text>,
        <Text key={6}>Implemented Cognito and custom authorization on API Gateway</Text>,
        <Text key={7}>Implemented various other AWS Services (SNS, SQS, DynamoDB, etc) with API Gateway</Text>,
        <Text key={8}>Developed shell scripts using the aws command to automate administrative tasks</Text>,
        <Text key={9}>Developed shell scripts to parse CloudWatch logs</Text>,
        <Text key={10}>Developed shell scripts to upload/download various data to/from Amazon S3</Text>,
        <Text key={11}>Analyse CloudWatch metrics to service issues with AWS Lambda, API Gateway</Text>,
        <Text key={12}>Develop relevant IAM roles and policies to ensure security and safety of AWS accounts</Text>,
        <Text key={13}>Launch different EC2 instance types</Text>,
        <Text key={14}>Snapshot EC2 instances and restore data from existing snapshots</Text>,
        <Text key={15}>Create custom AMIs and launch instances from the custom AMI</Text>,
        <Text key={16}>Set up periodic tasks in AWS using cron jobs</Text>,
        <Text key={17}>Auto scale instances using auto scaling groups</Text>,
        <Text key={18}>Implement dynamic and manual scaling of EC2 instances</Text>,
        <Text key={19}>Create VPC networks for various applications</Text>,
        <Text key={20}>Set up ELBs, NLBs and ALBs for load balancing</Text>,
        <Text key={21}>Create a CDN using CloudFront</Text>,
        <Text key={22}>Integrated CloudFront with Lambda@Edge functions</Text>,
        <Text key={23}>Insert, update, delete and query items from a DynamoDB data base</Text>,
        <Text key={24}>Integrate Lambda functions with DynamoDB using streams and via direct API calls</Text>,
        <Text key={25}>{
          "Assisted enterprise customers with developing and maintaining infrastructure in "
             + "accordance with the well-architected framework, ensuring efficient utilisation"
             + " of AWS resources while keeping running costs at a minimum"
             + "Worked with small to medium to enterprise level customers in developing their"
             + " cloud infrastructure in AWS"
        }</Text>,
        <Text key={26}>Achieved acknowledgement as Subject Matter Expert for Amazon API Gateway</Text>,
        <Text key={27}>{
          "Developed an internal tool in JavaScript for inspecting customer configuration of"
             + " Amazon API Gateway"
        }</Text>,
        <Text key={28}>{
          "Developed a raffle program in JavaScript for distributing prize money amaong team"
             + " members druing Diwali"}
        </Text>,
        <Text key={29}>{
          "I was also involved in hiring, training and various other knowledge transfer"
             + " activities as part of expanding our team"
        }</Text>,
        <Text key={30}>Helped build a team of 60+ engineers for the AWS serverless team in Bengaluru</Text>,
        <Text key={31}>Joined as Cloud Associated (Level 4) in Amazon Web Services in June 2016</Text>,
        <Text key={32}>Promoted to Cloud Engineer I and later to Level 5 and finally to Cloud Engineer II</Text>,
      ]}
    />
  </View>
);
