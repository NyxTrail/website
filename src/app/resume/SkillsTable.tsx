import {
  Text,
  View,
} from "@react-pdf/renderer";
import { tw } from "../components/tw";

type SkillsTableInput = {
  skills: string[]
};

export const SkillsTable = ({ skills } : SkillsTableInput) => (
  <View style={tw("font-firasans flex flex-row flex-wrap pl-4 pb-4")}>
    {
      skills.map((skill) => (
        <Text style={tw("min-w-40 text-sm")} key={skill}>{skill}</Text>
      ))
    }
  </View>
);
