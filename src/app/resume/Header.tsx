import {
  Link,
  Text,
  View,
} from "@react-pdf/renderer";
import { tw } from "../components/tw";

export const Header = () => (
  <View style={tw("border-b-2")}>
    <View style={tw("text-center")}>
      <Text style={tw("text-[18px]")}>Alan M Varghese</Text>
    </View>
    <View style={tw("flex flex-row py-1")}>
      <Text style={tw("text-xs my-0 py-0")}>
        Ph: <Link src="tel:+91-7812927954">+91 - 7812927954</Link> /{'\u00A0'}
        <Link src="tel:+91-7022812180">7022812180</Link>
      </Text>
      <View style={tw("grow items-center text-center")}>
        <Link style={tw("text-xs")} src="https://nyxtrail.digistorm.in">
          nyxtrail.digistorm.in
        </Link>
      </View>
      <Text style={tw("text-xs my-0 py-0")}>
        <Link src="mailto:alanmv01@gmail.com">alanmv01@gmail.com</Link>/<Link src="mailto:alan@digistorm.in">alan@digistorm.in</Link>
      </Text>
    </View>
  </View>
);
