import { Text, View } from "@react-pdf/renderer";
import { tw } from "../components/tw";

type SubHeadInput = {
  title: string
};

export const SubHead = ({ title }: SubHeadInput) => (
  <View style={tw("font-lato font-bold ml-2 text-base")}>
    <Text>
      {title}
    </Text>
  </View>
);
