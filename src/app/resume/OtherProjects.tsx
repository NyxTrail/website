import {
  Text,
  View,
} from "@react-pdf/renderer";
import { SectionHead } from "./SectionHead";
import { tw } from "../components/tw";

export const OtherProjects = () => (
  <View style={tw("pb-2")}>
    <SectionHead title="Other Projects & Interests" />
    <Text style={tw("px-5 text-justify")}>
      I love experimenting with various technologies and implementing various solutions
      in my home lab. I am an open source enthusiast who enjoys contributing to and developing FOSS projects.
    </Text>
  </View>
);
