import {
  Text,
  View,
} from "@react-pdf/renderer";
import { tw } from "../components/tw";
import { SectionHead } from "./SectionHead";

export const Objective = () => (
  <View style={tw("self-center w-4/5")}>
    <SectionHead title="Objective"/>
    <View>
    <Text style={tw("px-2 mx-2 text-justify")}>
      Highly skilled and motivated professional seeking opportunities to apply expertise in
      Linux, programming, and related technologies, contributing to team success and personal
      development.
    </Text>
    </View>
  </View>
);
