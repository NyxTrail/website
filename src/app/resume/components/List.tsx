import { Text, View } from "@react-pdf/renderer"
import { tw } from "@/app/components/tw";

export const List = ({ children }: any) => children

export const ListItem = ({ children } : any) => (
  <View style={tw("flex-row mb-2")}>
    <Text style={tw("font-light")}>• </Text>{children}
  </View>
);
