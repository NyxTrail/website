import {
  View,
} from "@react-pdf/renderer";
import { tw } from "../components/tw";
import { SectionHead } from "./SectionHead";
import { SubHead } from "./SubHead";
import { SkillsTable } from "./SkillsTable";

export const Skills = () => (
  <View style={tw("mb-2")}>
    <SectionHead title="Skills" />

    <SubHead title="Programming Languages & Others" />
    <SkillsTable skills={[
      "Javascript", "Java", "Kotlin", "Bash", "Python"
    ]} />

    <SubHead title="Cloud & AWS" />
    <SkillsTable skills={[
      "Kubernetes", "API Gateway", "Lambda", "EC2", "S3",
      "Cognito", "CloudFront", "Route 53", "IAM",
      "SNS", "SQS", "Step Functions", "Cloud Watch", "ECS",
      "Terraform", "Docker Compose"
    ]} />

    <SubHead title="Android" />
    <SkillsTable skills={[
      "Android Views", "Jetpack Compose", "Espresso"
    ]} />

    <SubHead title="Operating Systems" />
    <SkillsTable skills={[
      "Debian GNU/Linux", "Amazon Linux", "Red Hat Ent. Linux"
    ]} />
  </View>
);
