'use client'
import dynamic from "next/dynamic";

import {
  Document,
  Font,
  Page,
  View
} from "@react-pdf/renderer";

// import { PDFViewer } from "@react-pdf/renderer/lib/react-pdf.browser.min.js"
import { tw } from "../components/tw";
import { Header } from "./Header";
import { Objective } from "./Objective";
import { Photo } from "./Photo";
import { Skills } from "./Skills";
import { ProjectsAndExperience } from "./ProjectsAndExperience";
import { OtherProjects } from "./OtherProjects";
import { EducationAndCertifications } from "./EducationAndCertifications";

const PDFViewer = dynamic(async () => (await import("@react-pdf/renderer")).PDFViewer, { ssr: false })

Font.register({
  family: "FiraSans",
  src: `/assets/fonts/FiraSansRegular.ttf`,
  fontStyle: "normal",
  fontWeight: 400
});

Font.register({
  family: "Lato",
  fonts: [
    {
      src: `/assets/fonts/LatoRegular.ttf`,
      fontStyle: "normal",
      fontWeight: "normal",
    },
    {
      src: `/assets/fonts/LatoLight.ttf`,
      fontStyle: "normal",
      fontWeight: "light",
    },
    {
      src: `/assets/fonts/LatoBold.ttf`,
      fontStyle: "normal",
      fontWeight: "bold",
    }
  ]
});
  
export default function Resume () {
  return (
    <div className="h-screen mx-8 pt-4 h-[1000px] pb-96">
      <PDFViewer style={tw("h-[1000px] w-full")}>
        <Document>
          <Page size="A4" style={tw("border-l-8 border-cyan-600 bg-sky-50 p-12 font-lato text-sm")}>
            <Header />
            <View style={tw("flex-row mt-2")}>
              <Objective />
              <Photo />
            </View>
            <Skills />
            <ProjectsAndExperience />
            <OtherProjects />
            <EducationAndCertifications />
          </Page>
        </Document>
      </PDFViewer>
    </div>
  );
}
