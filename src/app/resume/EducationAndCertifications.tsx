import {
  Image,
  Text,
  View,
} from "@react-pdf/renderer";
import { SectionHead } from "./SectionHead";
import { List, ListItem } from "./components/List";
import { tw } from "../components/tw";

/*
import ckaUrl from "../../assets/cka-certified-kubernetes-administrator.png";
import ckadUrl from "../../assets/ckad-certified-kubernetes-application-developer.png";
*/

export const EducationAndCertifications = () => (
  <View>
    <SectionHead title="Education & Certifications" />

    <View style={tw("flex flex-row w-full pl-7")}>
      <View style={tw("w-[30rem]")}>
        <List >
          {
            [
              <Text key={0}>{
                "Certified Kubernetes Administrator (Linux Foundation, 2024-04-12 - 2027-04-13)"
              }</Text>,
              <Text key={1}>{
                "Certified Kubernetes Application Developer (Linux Foundataion, 2024-04-26 - "
                           + "2026-04-27)"
              }</Text>,
              <Text key={2}>Bachelor of Engineering, Computer Science and Technology (2011 - 2015)</Text>,
            ].map((item, index) => (
              <ListItem key={index}>
                {item}
              </ListItem>
            ))
          }
        </List>
      </View>

      <View style={tw("flex-row order-2 justify-end w-52")}>
        <View style={tw("bg-sky-50 border border-stone-400 p-2 m-1 rounded-md shadow-xl shadow-sky-500")}>
          <Image src={`/assets/cka-certified-kubernetes-administrator.png`} style={tw("w-20 h-20")}/>
        </View>
        <View style={tw("bg-sky-50 border border-stone-400 p-2 m-1 rounded-md shadow-xl shadow-sky-500")}>
          <Image src={`/assets/ckad-certified-kubernetes-application-developer.png`} style={tw("w-20 h-20")}/>
        </View>
      </View>
    </View>
  </View>
);
