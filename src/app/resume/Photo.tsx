import {
  Image,
  Link,
  View,
} from "@react-pdf/renderer";

/*
import photo from "../../assets/pic.jpg";
import LinkedIn from "../../assets/LI.png";
import github from "../../assets/github.png";
import gitlab from "../../assets/gitlab.png";
*/

import { tw } from "../components/tw";

export const Photo = () => (
  <View style={tw("border-2 border-indigo-200 self-center p-1 right-0 h-32")}>
    <View>
      <Image style={tw("border border-stone-600")} src={`/assets/pic.jpg`} />
    </View>
    <View style={tw("h-6 flex-row justify-center mt-2")}>
      <Link style={tw("h-6 w-6")} src="https://www.linkedin.com/in/alan-m-varghese">
        <Image src={`/assets/LI.png`} />
      </Link>
      <Link src="https://github.com/NyxTrail">
        <Image src={`/assets/github.png`} />
      </Link>
      <Link src="https://gitlab.com/NyxTrail">
        <Image style={tw("ml-1")} src ={`/assets/gitlab.png`} />
      </Link>
    </View>
  </View>
);
