'use client'
import { useThemeMode } from "flowbite-react";
import { useEffect, useState } from "react";

export default function Projects() {
  const themeMode = useThemeMode();

  const [githubImageURL, setGitHubImageURL] = useState('/assets/github-white.png')

  useEffect(() => {
    if(themeMode.mode === 'dark') {
      setGitHubImageURL('/assets/github-white.png');
    } else {
      setGitHubImageURL('assets/github.png');
    }
  }, [themeMode.mode])

  return (
    <div className="text-center pt-4">
      <h4 className="text-xl font-bold">Aksharam</h4>
      
      <div className="flex flex-col items-center md:flex-row md:justify-center pt-8 pb-96">
        <div className="w-[100px] h-[191px] md:w-[300px] md:h-[575px]">
          <img className="w-[100px] h-[191px] md:w-[300px] md:h-[575px]" src={'/assets/letters_tab_1.png'} />
          <div className="w-[100px] h-[191px] md:w-[300px] md:h-[575px] relative ring-4 md:ring-8 ring-slate-700 dark:ring-black rounded -top-[191px] -xl md:-top-[575px]"></div>
        </div>
        <div className="flex flex-col justify-center pl-8 pt-4 md:pt-0 text-justify w-[300px] md:w-[400px] lg:w-[600px]">
          <p>
            Aksharam is an open source android application that helps you
            learn an Indic script by letting you easily compare the alphabets (varnamala)
            of the language you wish to learn with the alphabets of another Indic script
            that you already know.
          </p>
          <p className="pt-4">
            Currently Kannada, Malayalam and Hindi are supported. Support for other languages
            is coming soon™. My vision for the app is to eventually support every Indic script
            that has Unicode support.
          </p>
          <div className="flex flex-row justify-center">
            <a href="https://play.google.com/store/apps/details?id=in.digistorm.aksharam">
              <img
                className="h-12 lg:h-24"
                src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
                alt="Get it on Google Play" />
            </a>
            <a href="https://f-droid.org/packages/in.digistorm.aksharam/">
              <img
                className="h-12 lg:h-24"
                src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
                alt="Get it on F-Droid" />
            </a>
            <a>
                <img
                  className="w-9 lg:w-20 pt-1 lg:pt-4"
                  src={githubImageURL}
                  alt="Source code" />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
