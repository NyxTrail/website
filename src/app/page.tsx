import Image from "next/image";

import Link from "next/link";
import { Profile } from "./components/profile/Profile";
import pic from "../../public/assets/pic.jpg";

export default function Home() {
  return (
    <div className="flex flex-col md:flex-row justify-center">
      <div className="w-[300px] md:w-[500px] order-2 md:order-1 mt-5 place-self-center px-4 text-justify">
        I am Alan M Varghese, a freelance developer creating aksharam, a learning app
        for Indic scripts.

        <br/> <br/>
        I have been a Cloud Engineer at Amazon Web Services, where I was an SME
        for Amazon API Gateway. I am also experience with other serverless offerings from
        AWS like Lambda, SNS, SQS, Lex, S3 and many others.

        <br/> <br/>
        I am an experienced Linux user and love playing around with FOSS technologies that
        I self-host on my Raspberry Pi.

        <br/> <br/>
        Aside from tech, I also enjoy video games, anime and books.

        <br/> <br/>
        Contact me at&nbsp;
        <Link className="text-blue-700" href="mailto:alan@digistormin">alan@digistorm.in</Link>
        &nbsp;if you would like to hire me for a project.
      </div>
      <div className="h-auto mt-5 order-1 place-self-center md:order-2">
        <Profile image={pic} />
      </div>
    </div>
  );
}
