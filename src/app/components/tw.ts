import { createTw } from "react-pdf-tailwind";
import { Style } from "@react-pdf/types";

type Tailwind = (input: string) => Style;

export const tw: Tailwind = createTw({
  theme: {
    fontFamily: {
      lato: ["Lato"],
      firasans: ["FiraSans"],
    },
    extend: {
      colors: {
        custom: "#bada55"
      },
    },
  },
});
