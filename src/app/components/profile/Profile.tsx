'use client'
import { useThemeMode } from "flowbite-react";
import { useEffect, useState } from "react";

interface ProfileProps {
  image: any
}

export const Profile = ({image}: ProfileProps) => {
  const themeMode = useThemeMode();
 
  const [githubImageURL, setGitHubImageURL] = useState('/assets/github-white.png')

  useEffect(() => {
    if(themeMode.mode === 'dark') {
      setGitHubImageURL('/assets/github-white.png');
    } else {
      setGitHubImageURL('assets/github.png');
    }
  }, [themeMode.mode])

  return (
    <div className="border border-violet-400 shadow-md shadow-slate-400 dark:shadow-slate-950 rounded-md mx-auto p-4 w-32">
      <img src={image.src} />
      <div className="flex mt-2 h-6 mx-1">
        <a href="https://www.linkedin.com/in/alan-m-varghese/">
          <img className="h-6" src={'/assets/LI.png'} alt="Link to LinkedIn profile" />
        </a>
        <a href="https://github.com/NyxTrail">
            <img className="h-6 mx-1" src={githubImageURL} alt="github" />
        </a>
        <a href="https://gitlab.com/NyxTrail">
          <img className="dark: h-6" src={'/assets/gitlab.png'} />
        </a>
      </div>
    </div>
  );
}
